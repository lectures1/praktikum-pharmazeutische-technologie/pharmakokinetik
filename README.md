# Praktikum Pharmazeutische Technologie, 
## Versuch 09: Pharmakokinetische Simulationen

### Übersicht

1. Arbeiten mit [Jupyter](https://jupyter.org)-Notebooks und [Python](https://www.python.org)-Grundlagen
2. intravenöse Applikation
  - Bolusinjektion eines hydrophilen Arzneitstoffes (Einkompartimentmodell)
  - Bolusinjektion eines lipophilen Arzneistoffes (Zweikompartimentmodell)
  - Infusion eines hydrophilen Arzneitstoffes
  - Infusion eines lipophilen Arzneistoffes
3. perorale Applikation
  - einfach
  - mehrfach



### Lehr- und Lernziele

Durch *pharmakokinetische Simulationen* in Kompartimentmodellen wissen
die Studierenden, wie sich *pharmakokinetische Parameter* auf die
Bioverfügbarkeit einer Arzeiform auswirken. Damit sollen sie in die
Lage versetzt werden, pharmakokinetische Prozesse zu verstehen und
pharmakokinetsiche Aspekte bei der Entwicklung von Arzneimitteln zu
berücksichtigen, Darreichungsformen hinsichtlich ihrer Pharmakokinetik
zu bewerten und richtig anzuwenden.

Das *kognitive Ziel* dieser Versuchsreihen ist ganz bewusst nur auf
die Kompetenzstufe Wissen ausgelegt. Dieses Wissen ist Grundlage zum
Verständnis der Vorgänge und für den Erwerb höherer Kompetenzstufen.
