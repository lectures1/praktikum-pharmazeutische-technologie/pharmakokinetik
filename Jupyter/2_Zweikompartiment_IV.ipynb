{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Zweikompartiment-Modell"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Für diesen Versuch werden wir unser Modell aus Versuch 1 um zwei Aspekte erweitern:\n",
    "\n",
    "1. Wir werden ein **zweites Kompartiment** einführen, damit wir *Verteilungsprozesse* \n",
    "   für *lipophile* Wirkstoffe modellieren können.\n",
    "2. Wir werden ein fiktives **Arzneiform**-Kompartiment einführen, damit wir das\n",
    "   Freisetzungsverhalten aus der Arzneiform besser beobachten können.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Außerdem werden wir die Ergebnisse der Simulation in einem speziellen\n",
    "Datenformat, einem [*data\n",
    "frame*](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html),\n",
    "speichern. Dazu benötigen wir neben NumPy auch die Bibliothek\n",
    "[*pandas*](https:°pandas.pydata.org) (*Python Data Analysis Library*).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Und schließlich kapseln wir die Simulation in eine eigene\n",
    "**Funktion**. Das macht es viel einfacher, die Auswirkungen der\n",
    "einzelnen Parameter auf das System zu untersuchen.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## intravasale Bolusapplikation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Modell"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Diagramm Zweikompartimentmodell mit Arzneiform](images/intravasal2_B.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "verwendete Symbole:\n",
    "\n",
    "|        |                                           |\n",
    "|:------:|:----------------------------------------- |\n",
    "| $D$    | Wirkstoffdosis                            |\n",
    "| $m_A$  | Wirkstoffmasse in der Arzneiform |\n",
    "| $m_1$  | Wirkstoffmasse im Zentralkompartiment     |\n",
    "| $m_2$  | Wirkstoffmasse im peripheren Kompartiment     |\n",
    "| $m_E$  | eliminierte Wirkstoffmasse                |\n",
    "| $\\infty$  | Bolusinvasion |\n",
    "| $k_e$  | Eliminationsgeschwindigkeitskonstante     |\n",
    "| $k_{12}, k_{21}$  | Verteilungsgeschwindigkeitskonstanten     |\n",
    "| AUC    | Inhalt der Fläche unter der Wirkstoffspiegelkurve |\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Differentialgleichungen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\\begin{align}\n",
    "  \\frac{d m_A}{dt} &= -m_A \\\\\n",
    "  \\frac{d m_1}{dt} &= \\hphantom{-}m_A - k_{12}\\cdot m_1 + k_{21}\\cdot m_2 - k_e\\cdot m_1 \\\\\n",
    "  \\frac{d m_2}{dt} &= \\hphantom{-}k_{12}\\cdot m_1 - k_{21}\\cdot m_2\\\\\n",
    "  \\frac{d m_E}{dt} &= \\hphantom{-}k_e\\cdot m_1\n",
    "\\end{align}\n",
    "\n",
    "(Die Masse $m_A$ hat bei einer Bolusinjektion zum Zeitpunkt $t=0$ den\n",
    "Wert $D$ und ist danach immer 0.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Finite-Differenzen-Methode"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Jetzt wird klar, warum wir bei der Finite-Differenzen-Methode immer\n",
    "nur den kurzen Zeitraum $\\varepsilon$ betrachten: In diesem Zeitraum\n",
    "können wir die Kompartimente *entkoppeln* und die Übergänge\n",
    "berechnen. Am Ende dieses Zeitintervalls werden die Massen in den\n",
    "einzelnen Kompartimenten aktualisiert. Das macht das Modell\n",
    "ausgesprochen flexibel. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die Arzneistoffmassen, die im Zeitraum $\\varepsilon$ zwischen den einzelnen\n",
    "Kompartimenten fließen, sind:\n",
    "\\begin{align}\n",
    "\\Delta_{A1} &= m_A(t) \\\\\n",
    "\\Delta_{12} &= m_1(t)\\cdot\\bigl(1 - e^{-k_{12} \\cdot\\varepsilon}\\bigr) \\\\\n",
    "\\Delta_{21} &= m_2(t)\\cdot\\bigl(1 - e^{-k_{21} \\cdot\\varepsilon}\\bigr) \\\\\n",
    "\\Delta_{1E} &= m_1(t)\\cdot\\bigl(1 - e^{-k_e \\cdot\\varepsilon}\\bigr)    \n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Programmierung in Python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Wieder legen wir zuerst den **Simulationsparameter** `t_max`\n",
    "fest, damit wir die Ergebnisvariablen entsprechend dimensionieren können."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t_max = 500   # simulation time / minutes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die Wirkstoffverläufe in den einzelnen Kompartimenten implementieren\n",
    "wir diesmal als\n",
    "[Zeitreihen](https://pandas.pydata.org/pandas-docs/stable/reference/series.html)\n",
    "aus der *pandas*-Bibliothek. Sie verhalten sich ähnlich wie\n",
    "[Numpy-Arrays](https://numpy.org/doc/stable/reference/generated/numpy.array.html)\n",
    "bieten aber einige Vorteile, z. B. lassen sie sich leicht zu einem\n",
    "[*Dataframe*](https://pandas.pydata.org/pandas-docs/stable/getting_started/dsintro.html#dataframe)\n",
    "zusammenfassen oder [graphisch darstellen](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.plot.html#pandas.Series.plot).\n",
    "\n",
    "(Für einen Vergleich zwischen Numpy-Array und pandas-Dataframe siehe [Artikel von Eric van\n",
    "Rees](https://medium.com/@ericvanrees/pandas-series-objects-and-numpy-arrays-15dfe05919d7).)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Das zentrale Kompartiment nennen wir jetzt $C_1$ und das periphere Kompartiment $C_2$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "index = np.arange(t_max + 1)    # {0, 1, 2, ..., t_max}\n",
    "A   = pd.Series(0, index=index, dtype=np.float)   # Feld mit (t_max + 1) Nullen\n",
    "C1  = pd.Series(0, index=index, dtype=np.float) \n",
    "C2  = pd.Series(0, index=index, dtype=np.float) \n",
    "E   = pd.Series(0, index=index, dtype=np.float)\n",
    "AUC = pd.Series(0, index=index, dtype=np.float)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Als nächstes werden die **Systemparameter** *Dosis* $D$ und\n",
    "die *Geschwindigkeitskonstanten* $k_{12}$, $k_{21}$ und $k_e$ festgelegt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "D    = 100      # dose / mg\n",
    "k_e  = 0.004    # rate constant of elimination / (per minute)\n",
    "k_12 = 0.100    # per minute\n",
    "k_21 = 0.001    # per minute\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Mit den *Geschwindigkeitskonstanten* sind auch die\n",
    "entsprechenden *Minutenfaktoren* bestimmt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mf_12 = 1 - np.exp(-k_12)\n",
    "mf_21 = 1 - np.exp(-k_21)\n",
    "mf_e  = 1 - np.exp(-k_e)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Als **Anfangsbedingung** muss die Arzneistoffmasse im fiktiven\n",
    "Arzneiformkompartiment für den Zeitpunkt $t=0$ auf $D$ gesetzt\n",
    "werden. Das geht bei `Series`-Objekten genau wie bei NumPy-*Arrays*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A[0] = D"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Für die eigentliche **Simulation** lassen wir die Indexvariable `t`\n",
    "einfach den Index der `Series`-Objekte durchlaufen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Zunächst werden die *Massenflüsse* $\\Delta$ im entkoppelten System\n",
    "berechnet; danach werden die *Arzneistoffmassen* in den einzelnen\n",
    "Kompartimenten aktualisiert. Die AUC im zentralen Kompartiment wird wieder über die\n",
    "[Trapezregel](https://de.wikipedia.org/wiki/Trapezregel) berechnet."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for t in A.index:\n",
    "    d_a1  = A[t]\n",
    "    C1[t] += d_a1\n",
    "\n",
    "    d_12 = C1[t]*mf_12\n",
    "    d_21 = C2[t]*mf_21\n",
    "    d_1e = C1[t]*mf_e\n",
    "\n",
    "    A[t+1]  = A[t]  - d_a1\n",
    "    C1[t+1] = C1[t] - d_12 + d_21 - d_1e\n",
    "    C2[t+1] = C2[t] + d_12 - d_21\n",
    "    E[t+1]  = E[t]                + d_1e\n",
    "    \n",
    "    AUC[t+1] = AUC[t] + (C1[t] + C1[t+1])/2  # trapezoidal rule\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Für das Ergebnis fassen wir jetzt die Felder mit den Massenverläufen in einem\n",
    "*Dataframe* zusammen; jede Spalte bekommt dabei einen Namen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result = pd.DataFrame({'Arzneiform': A, 'Blutplasma': C1, 'Gewebe': C2, 'Elimination': E})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ein Vorteil von *Dataframes* ist das voreingestellte Ausgabeformat:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Kapseln in eine Funktion"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Diese Rechenschritte packen wir jetzt in eine eigene Funktion `simulate_B`. Sie kann dann einfach mit den passenden Simulationsparametern aufgerufen werden und liefert den *data frame* als Funktionswert zurück.\n",
    "\n",
    "Die Parameterwerte für `D` und `t_max` bekommen eine geeignete Voreinstellung; sie können dann beim Aufrufen der Funktion auch weggelassen werden."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def simulate_B(k_12, k_21, k_e, D=100, t_max=500):\n",
    "\n",
    "    # initialisation\n",
    "    index = np.arange(t_max + 1)    # {0, 1, 2, ..., t_max}\n",
    "    A   = pd.Series(0, index=index, dtype=np.float)   # Feld mit (t_max + 1) Nullen\n",
    "    C1  = pd.Series(0, index=index, dtype=np.float) \n",
    "    C2  = pd.Series(0, index=index, dtype=np.float) \n",
    "    E   = pd.Series(0, index=index, dtype=np.float)\n",
    "    AUC = pd.Series(0, index=index, dtype=np.float)\n",
    "\n",
    "    # minute factors\n",
    "    mf_12 = 1 - np.exp(-k_12)\n",
    "    mf_21 = 1 - np.exp(-k_21)\n",
    "    mf_e  = 1 - np.exp(-k_e)\n",
    "\n",
    "    # initial condition\n",
    "    A[0] = D\n",
    "\n",
    "    # simulation\n",
    "    for t in A.index:\n",
    "        d_a1  = A[t]\n",
    "        C1[t] += d_a1\n",
    "\n",
    "        d_12 = C1[t]*mf_12\n",
    "        d_21 = C2[t]*mf_21\n",
    "        d_1e = C1[t]*mf_e\n",
    "\n",
    "        A[t+1]  = A[t]  - d_a1\n",
    "        C1[t+1] = C1[t] - d_12 + d_21 - d_1e\n",
    "        C2[t+1] = C2[t] + d_12 - d_21\n",
    "        E[t+1]  = E[t]                + d_1e\n",
    "        \n",
    "        AUC[t+1] = AUC[t] + (C1[t] + C1[t+1])/2  # trapezoidal rule\n",
    "\n",
    "    # creating the result\n",
    "    return pd.DataFrame({'Arzneiform': A, 'Blutplasma': C1, 'Gewebe': C2, 'Elimination': E})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Jetzt können wir `simulate_B` einfach mit verschiedenen Parametern aufrufen. Zuerst reproduzieren wir das Ergebnis von oben mit $k_{12} = 0{,}100 \\mathrm{min}^{-1}$, $k_{21} = 0{,}001 \\mathrm{min}^{-1}$ und $k_e = 0{,}0.004 \\mathrm{min}^{-1}$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "simulate_B(0.100, 0.001, 0.004)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualisierung"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "\n",
    "from pylab import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Auch das graphische Ausgabeformat für *data frames* ist bereits sinnvoll voreingestellt:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result = simulate_B(0.050, 0.010, 0.004)\n",
    "result.plot()\n",
    "title('Bolusinjektion im Zweikompartimentmodell')\n",
    "xlabel('t/min')\n",
    "ylabel('m/mg')\n",
    "grid(True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Wir wollen die Arzneistoffverläufe in den Kompartimenten aber einzeln darstellen. Außerdem überlassen wir die Fleißarbeit wieder einer Funktion: Die Funktion `visualise_B` akzeptiert die gleichen Parameter wie `simulate_B`, führt die Simulation durch und stellt den Konzentrationsverlauf im Blutplasma und im Gewebe dar:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def visualise_B(k_12, k_21, k_e, D=100, t_max=500):\n",
    "    result = simulate_B(k_12, k_21, k_e, D, t_max)\n",
    "    \n",
    "    figure(figsize=(10,6)) # create new figure\n",
    "    title(\"Wirkstoffverteilung im Zweikompartimentmodell\")\n",
    "    result[\"Blutplasma\"].plot()\n",
    "    result[\"Gewebe\"].plot()\n",
    "    legend() # add legend\n",
    "    grid(True)\n",
    "    xlabel(\"t/min\")\n",
    "    ylabel(\"m/mg\")\n",
    "    \n",
    "visualise_B(0.100, 0.001, 0.004)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Das Gewebekompartiment verhält sich wie ein Reservoir. Es kann über lange Zeit Wirkstoff in das zentrale Kompartiment nachliefern und für eine relativ konstanten Plasma-Wirkstoff-Konzentration sorgen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "visualise_B(0.050, 0.01, 0.004)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Aufgaben"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Aufgabe 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Variieren Sie die Geschwindigkeitskonstanten. Mit welchen Parametern würde sich möglichst schnell ein möglichst konstanter Plasmaspiegel von 20 mg einstellen?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Aufgabe 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Können Sie mit diesem Modell auch die Verhältnisse im Ein-Kompartimentmodell beschreiben? Wenn ja, wie?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Infusion"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Im Folgenden werden wir betrachten, wie sich die Wirkstoffspiegel in den einzelnen Kompartimenten ändern, wenn der Wirkstoff nicht mehr als Bolus, sondern mit einer konstanten Rate über einen längeren Zeitraum appliziert wird (Infusion, Kinetik 0. Ordnung).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Differentialgleichung"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Die Rate, mit der der Wirkstoff (aus der Arzeneiform) in das zentrale Kompartiment strömt, ist konstant – jedenfalls solange sich noch Arzneistoff in der Arzneiform befindet:\n",
    "\n",
    "$$ \\frac{d m_A}{dt} = -{}^0\\!k_a $$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def simulate_I(k_a, k_12, k_21, k_e, D=100, t_max=500):\n",
    "\n",
    "    # initialisation\n",
    "    index = np.arange(t_max + 1)    # {0, 1, 2, ..., t_max}\n",
    "    A   = pd.Series(0, index=index, dtype=np.float)   # Feld mit (t_max + 1) Nullen\n",
    "    C1  = pd.Series(0, index=index, dtype=np.float) \n",
    "    C2  = pd.Series(0, index=index, dtype=np.float) \n",
    "    E   = pd.Series(0, index=index, dtype=np.float)\n",
    "    AUC = pd.Series(0, index=index, dtype=np.float)\n",
    "\n",
    "    # minute factors\n",
    "    mf_12 = 1 - np.exp(-k_12)\n",
    "    mf_21 = 1 - np.exp(-k_21)\n",
    "    mf_e  = 1 - np.exp(-k_e)\n",
    "\n",
    "    # initial condition\n",
    "    A[0] = D\n",
    "\n",
    "    # simulation\n",
    "    for t in A.index:\n",
    "        d_a1  = k_a if k_a < A[t] else A[t] # <-- nur diese Zeile geändert\n",
    "        C1[t] += d_a1\n",
    "\n",
    "        d_12 = C1[t]*mf_12\n",
    "        d_21 = C2[t]*mf_21\n",
    "        d_1e = C1[t]*mf_e\n",
    "\n",
    "        A[t+1]  = A[t]  - d_a1\n",
    "        C1[t+1] = C1[t] - d_12 + d_21 - d_1e\n",
    "        C2[t+1] = C2[t] + d_12 - d_21\n",
    "        E[t+1]  = E[t]                + d_1e\n",
    "        \n",
    "        AUC[t+1] = AUC[t] + (C1[t] + C1[t+1])/2  # trapezoidal rule\n",
    "\n",
    "    # creating the result\n",
    "    return pd.DataFrame({'Arzneiform': A, 'Blutplasma': C1, 'Gewebe': C2,\n",
    "                         'Elimination': E, 'AUC': AUC})\n",
    "\n",
    "def visualise_I(k_r, k_12, k_21, k_e, D=100, t_max=500):\n",
    "    result = simulate_I(k_r, k_12, k_21, k_e, D, t_max)\n",
    "    \n",
    "    figure(figsize=(10,6)) # create new figure\n",
    "    title(\"Wirkstoffverteilung im Zweikompartimentmodell\")\n",
    "    result[\"Blutplasma\"].plot()\n",
    "    result[\"Gewebe\"].plot()\n",
    "    legend() # add legend\n",
    "    grid(True)\n",
    "    xlabel(\"t/min\")\n",
    "    ylabel(\"m/mg\")\n",
    "    \n",
    "visualise_I(0.5, 0.000, 0.001, 0.01, D=150)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Aufgaben"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Aufgabe 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Welche Dimension hat $k_R$? (Ersatzweise dürfen Sie auch eine geeignete Einheit angeben.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Aufgabe 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Erklären Sie, was die geänderte Zeile\n",
    "```d_a1  = k_a if k_a < A[t] else A[t]```\n",
    "in der Definition von `simulate_I` bewirkt."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Aufgabe 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Erklären Sie den Blutspiegelverlauf im letzten Beispiel."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Aufgabe 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ein hydrophiler Arneistoff mit einer Eliminationsgeschwindigkeitskonstanten von $k_e = 0{,}03 \\text{min}^{-1}$ soll so infundiert werden, dass sich eine Wirkstoffmasse von 20,0 mg im Blutplasma einstellt und nach Erreichen über vier Stunden aufrechterhalten wird. Wie müssen Sie die Infusionsrate $k_r$ und die Dosis $D$ wählen? Nach welcher Zeit ist der Blutspiegel erreicht?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Aufgabe 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Wie ändert sich der Blutspiegelverlauf für einen lipophilen Wirkstoff?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "name": "2_Zweikompartiment_IV.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
