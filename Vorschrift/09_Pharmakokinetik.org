#+Title: V09 — Pharmakokinetische Simulationen
#+Language: de-de
#+Bibliography: references plain

* Org Stuff                                                        :noexport:
  :PROPERTIES:
  :ORDERED:  t
  :END:
#+startup: align
#+latex_header: \usepackage{f11script}
#+latex_header: \usepackage{tikz,pgfplots}
#+LaTeX_header: \usepackage[eulergreek]{sansmath}

#+LaTeX_header: \sisetup{range-phrase={--},locale = DE}
#+LaTeX_header: \usetikzlibrary{positioning}
#+LaTeX_header: \newcommand{\cmax}{\ensuremath{c_{\text{max}}}}
#+LaTeX_header: \newcommand{\tmax}{\ensuremath{t_{\text{max}}}}

#+LaTeX_header: \modul{Praktikum Pharmazeutische Technologie}
#+LaTeX_header: \expno{09}
#+LaTeX_header: \experiment{Pharmakokinetische Simulationen}
#+LaTeX_header: \version{1}

#+LaTeX_header: \DeclareUnicodeCharacter{2009}{\,}

#+LaTeX_header: \reversemarginpar
#+LaTeX_header: \newcommand{\wawowo}[2][black]{\marginpar{\color{#1}\Large\scshape\bfseries{\hspace*{.6em}#2}?}}
#+LaTeX_header: \newcommand{\wwwas}{\wawowo[thred]{was}}
#+LaTeX_header: \newcommand{\wwwomit}{\wawowo[thorange]{womit}}
#+LaTeX_header: \newcommand{\wwwozu}{\wawowo[thviolet]{wozu}}

#+LaTeX_header: \newcommand{\kzero}{\ensuremath{{}^0\!k}}
#+LaTeX_header: \newcommand{\kone}{\ensuremath{{}^1\!k}}


* Vorbereitung und Aufgabenstellung

** Erforderliche theoretische Kenntnisse

- Sie kennen das [[https://flexikon.doccheck.com/de/Pharmakokinetik][LADME-Modell]].
- Sie kennen die pharmakokinetischen Parameter für die *Bioverfügbarkeit*.

- Sie können mit Kinetiken [[https://flexikon.doccheck.com/de/Reaktion_0._Ordnung][0. Ordnung]] und [[https://flexikon.doccheck.com/de/Reaktion_1._Ordnung][1. Ordnung]] arbeiten.
- Sie können Differentialgleichungen 1. Ordnung lesen und verstehen.

** Aufgabenstellung

# Womit
Sie *simulieren* das Verhalten von Wirkstoffen in
pharmakokinetischen Kompartimentmodellen.

Dabei untersuchen Sie den Einfluss der Dosierung, 
#+LaTeX: \wwwomit
der Übergangskinetiken (0. und 1. Ordnung) zwischen den Kompartimenten
zusammen mit ihren Parametern (Geschwindigkeitskonstanten) 
auf die zeitliche und räumliche Verteilung 
von Arzneistoffen im Körper.

# Was
Nach Absolvieren der Versuche *wissen* Sie, wie die
#+LaTeX: \wwwas
/pharmakokinetischen Eigenschaften/ eines Wirkstoffes zusammen mit dem
/Freisetzungsverhalten/ der Arzneiform und dem /Dosierungschema/ die
Bioverfügbarkeit einer pharmazeutischen Formulierung beeinflussen.

# Wozu
Damit werden Sie in die Lage versetzt, die zugrundeliegenden
#+LaTeX: \wwwozu
pharmakokinetischen Zusammenhänge zu *verstehen* und die Auswirkungen
von Rezepturentscheidungen bei der Formulierungsentwicklung auf die
/Bioverfügbarkeit/ und damit auf die /Wirksamkeit/ des Arzneimittels zu
*steuern*.


* Grundlagen

** Modelle 

Die Naturwissenschaften versuchen, die physikalische Realität zu
beschreiben und mehr oder weniger gute /Vorhersagen/ zu
Veränderungsprozessen in der physikalischen Realität zu machen.

Dazu werden Modelle verwendet. Mit ihnen wird die komplexe Realität
/abstrahiert/, d. h. Details werden weggelassen. Die Kunst dabei ist,
zu erkennen und zu entscheiden, welche Details weggelassen werden
dürfen, ohne die Vorhersagekraft der Modelle allzusehr zu
beschränken. Das bedeutet auch, dass man für verschiedene
Fragestellungen mitunter unterschiedliche Modelle einsetzen muss.

Häufig können die Modelle mit Hilfe von Gleichungen beschrieben und
mathematisch /analysiert/ werden. (Da wir vor allem Veränderungen
interessant finden, handelt es sich häufig um
Differentialgleichungen.) Eine andere Möglichkeit, Modelle zu
untersuchen, ist die /Simulation/.

Ziel von Analyse oder Simulation ist es, /Vorhersagen/ zu machen oder
/Erklärungen/ zu finden. (Ingenieure ermitteln mit diesen Methoden
auch geeignete /Designs/.)

Ein naturwissenschaftliches Modell ist immer /falsifizierbar/. Man
kann es auf die Probe stellen. Wenn die Vorhersagen eines Modells auch
wirklich eintreffen, gilt es als /validiert/.

Wenn die Vorhersagen eines Modells zwar prinzipiell richtig aber nicht
genau genug sind, kann man das Modell verfeinern, indem man weitere
Details berücksichtigt. Diesen Prozess nennt man /iteratives Modellieren/.

In diesem Praktikumsversuch wollen wir das Verhalten der Modelle
bzw. der zugrundeliegenden Gleichungen mit Hilfe von
Computerprogrammen simulieren.


** Pharmakokinetische Modelle

Entscheidend für die Wirksamkeit eines Arzneimittels ist neben den
pharmakodynamischen Eigenschaften des Wirkstoffes seine
/Bioverfügbarkeit/ – das /Ausmaß/ und die /Geschwindigkeit/, mit
der der Wirkstoff an seinen Wirkort gelangt.

*** LADME-Modell

Die der Bioverfügbarkeit zu Grunde liegenden Prozesse sind auf der
physiologischen Ebene äußerst komplex und längst nicht vollständig
bekannt. In einem gängigen Modell, dem ADME-Modell, werden die
pharmakokinetischen Prozesse in Absorption, Distribution,
Metabolisierung und Exkretion der Wirkstoffe aufgeteilt:

#+begin_src dot :file images/adme.png :exports none
digraph {
  rankdir=LR;
  node [shape=box width=1.8];
  Absorption -> Distribution -> Metabolisierung -> Exkretion;
}
#+end_src

#+begin_center
#+begin_export latex
\begin{tikzpicture}[node distance=10mm]
  \tikzstyle{comp} = [rectangle, draw, align=center,
    minimum width=30mm, minimum height=12mm]
  
  \node[comp]  (A) at (0, 0) {Resorption\\(\textsl{Absorption})};
  \node[comp, right=of A] (D) {Verteilung\\(\textsl{Distribution})};
  \node[comp, right=of D] (M) {Metabolisierung};
  \node[comp, right=of M] (E) {Exkretion};
  
   \draw[thick,->] (A) -- (D);
   \draw[thick,->] (D) -- (M);
   \draw[thick,->] (M) -- (E);
\end{tikzpicture}
#+end_export
#+end_center

In der pharmazeutischen Technologie wird die Bioverfügbarkeit vor
allem über die /Freisetzung/ (engl. /liberation/) des Wirkstoffes aus der
Arzneiform gesteuert. Daher verwenden wir das entsprechend erweiterte
LADME-Modell:

#+begin_src dot :file images/ladme.png :exports none
digraph {
  rankdir=LR;
  node [shape=box width=1.8];
  Liberation -> Absorption -> Distribution -> Metabolisierung -> Exkretion;
}
#+end_src

#+begin_center
#+begin_export latex
\begin{tikzpicture}[node distance=10mm]
  \tikzstyle{comp} = [rectangle, draw, align=center,
    minimum width=30mm, minimum height=12mm]
  
  \node[comp]  (A) at (0, 0) {Resorption\\(\textsl{Absorption})};
  \node[comp, right=of A] (D) {Verteilung\\(\textsl{Distribution})};
  \node[comp, right=of D] (M) {Metabolisierung};
  \node[comp, right=of M] (E) {Exkretion};
  \node[comp, above=of A] (L) {Freisetzung\\(\textsl{Liberation})};
  
   \draw[thick,->] (A) -- (D);
   \draw[thick,->] (D) -- (M);
   \draw[thick,->] (M) -- (E);
   \draw[thick,->] (L) -- (A);
\end{tikzpicture}
#+end_export
#+end_center


*** Kompartimentmodelle

/Drug delivery/-Systeme und der menschliche (oder tierische) Körper
werden als ein oder mehrere fiktive Räume modelliert; diese Räume
werden /Kompartimente/ genannt. 

Die Modellannahmen für ein Kompartiment sind
1) ein Wirkstoff verteilt sich /unverzüglich/ im Kompartiment,
2) ein Wirkstoff verteilt sich /homogen/ im Kompartiment,
3) der Transport von Wirkstoffen in das und aus dem Kompartiment
   folgen jeweils einer /einheitlichen Kinetik/.


** Übergänge

Die Übergänge zwischen den Kompartimenten werden durch
Differentialgleichungen beschrieben.  Dabei muss die Massenbilanz
immer aufgehen.

*** Kinetik 0. Ordnung

Bei einer Kinetik 0. Ordnung hängt die Arzneistoffmenge, die von einem
in ein anderes Kompartiment übergeht, nicht von der Arzneistoffmasse \(m_S\)
im Ursprungskompartiment (/source/) ab; sie ist konstant \(\kzero\).

#+begin_export latex
\begin{equation}
  \label{eq:ordnung-0}
  \frac{dm}{dt} = \begin{cases}
    \kzero & \text{für $\kzero < m_S$} \\
    m_S    & \text{sonst}
  \end{cases}
\end{equation}
#+end_export

*** Kinetik 1. Ordnung

Bei einer Kinetik 1. Ordnung ist die die Arzneistoffmenge, die von
einem in ein anderes Kompartiment übergeht, proportional zur
Arzneistoffmasse \(m_S\) im Ursprungskompartiment; die
Proportionalitätskonstante \(\kone\) wird /Geschwindigkeitskonstante/
genannt.

#+begin_export latex
\begin{equation}
  \label{eq:ordnung-1}
  \frac{dm}{dt} = \kone\cdot m_S
\end{equation}
#+end_export


** Lösung der Differentialgleichungssysteme


*** Beispiel: Einkompartimentmodell mit extravasaler Applikation
<<example>>

#+begin_export latex
\begin{tikzpicture}[node distance=25mm]
  \tikzstyle{comp} = [rectangle, draw, align=center,
    minimum width=35mm, minimum height=15mm]
  \tikzstyle{extra} = [comp, dashed]
  \tikzstyle{dform} = [comp, dashed, rounded corners]  
  
  \node[comp]  (P) at (0, 0) {Organismus\\$m_P$};
  \node[extra, right=of P] (E) {Exkretion\\$m_E$};
  \node[dform, left=of P] (A) {Arzneiform\\$m_A$};
  
   \draw[thick,->] (A) -- node[above] {$D\quad k_i$} (P);
   \draw[thick,->] (P.east) -- node[above] {$k_e$} (E.west);
\end{tikzpicture}
#+end_export

*** Differentialgleichungen
<<dgl>>

#+begin_export latex
\begin{align}
  \label{eq:dgl}
  \frac{dA}{dt} &= -k_i \cdot m_A \\
  \frac{dP}{dt} &= \hphantom{-}k_i \cdot m_A - k_e \cdot m_P \\
  \frac{dE}{dt} &= \hphantom{- k_i \cdot m_A - {}} k_e \cdot m_P
\end{align}
#+end_export

*** Analytische Lösungen

Für Systeme linearer Differentialgleichungen kann man grundsätzlich
[[https://de.wikipedia.org/wiki/Fundamentalsystem_(Mathematik)][analytische Lösungen]] finden. Außer in den einfachsten Fällen sind
diese aber ziemlich komplex und wenig flexibel, wenn die Modelle
erweitert werden sollen.

Für das Einkompartimentmodell mit extravasaler Applikation ist die
/Bateman/-Funktion die Lösung für den Arzneistoffverlauf im
Plasmakompartiment.

#+begin_export latex
\begin{equation}
  \label{eq:bateman}
  m(t) = \frac{D}{f}\,\frac{k_i}{k_i - k_e}\,\Big(e^{-k_e t} - e^{-k_i t}\Bigr)
\end{equation}
#+end_export

*** Finite-Differenzen-Methode

Die [[https://de.wikipedia.org/wiki/Finite-Differenzen-Methode][Finite-Differenzen-Methode]] ist ein numerisches Lösungsverfahren,
bei dem Systeme von Differentialgleichungen über kurze Zeiträume
enkoppelt betrachtet werden.

Unabhängig von Veränderungen in anderen Kompartimenten werden für
diesen kurzen Zeitraum die Massen berechnet, die aus einem
Kompartiment in ein anderes übergehen. Meistens kann dazu die einzeln
betrachtete Differentialgleichung analytisch gelöst werden. Danach
werden die Massen in allen Kompartimenten aktualisiert.

Im Beispiel aus Abschnitt [[example]] beträgt die
Arzneistoffmasse \(\Delta_i\), die in einem Zeitraum \(\varepsilon\)
aus der Arzneiform abfließt
#+begin_export latex
\begin{align}
  m_A(t + \varepsilon) &= m_A(t)\cdot e^{-k_i \cdot\varepsilon} \\\nonumber
                     &= m_A(t) - \Delta_i\\
\Delta_i             &= m_A(t)\cdot\bigl(1 - e^{-k_i \cdot\varepsilon}\bigr)\\
\noalign{Genauso wird bei der Finite-Differenzen-Methode auch die aus
  dem Organismus eliminierte Masse berechnet:}
\Delta_e             &= m_P(t)\cdot\bigl(1 - e^{-k_e \cdot\varepsilon}\bigr)
\end{align}
#+end_export
Beachten Sie, dass die im Zeitraum \(\varepsilon\) resorbierte Masse dabei
unberücksichtigt bleibt. (Der dadurch entstehende Fehler ist der Preis dafür, 
dass wir die Differentialgleichungen aus [[dgl]] einzeln und nicht mehr im System
lösen müssen.)

Nach der Berechnung der einzelnen Veränderungen unabhängig voneinander wird das
System aus Abschnitt [[dgl]] wieder zusammengeführt:
#+begin_export latex
\begin{align}
  m_A(t + \varepsilon) &\mathbin{\leftarrow} m_A(t) - \Delta_i\\
  m_P(t + \varepsilon) &\mathbin{\leftarrow} m_P(t) + \Delta_i - \Delta_e.
\end{align}
#+end_export



* Versuch

** Versuchsbeschreibung

In diesem Praktikumsversuch untersuchen Sie das pharmakokinetische
Verhalten eines Arzneistoffes in verschiedenen Kompartimentmodellen.

** Benötigte Utensilien

- Computer mit Internetanschluss,
- [[https://www.th-koeln.de/hochschule/vpn---virtual-private-network_26952.php][VPN-Zugang]] zum Campusnetz der TH-Köln (Hinweise zur Einrichtung
  unter [[https://www.th-koeln.de/mam/downloads/deutsch/hochschule/organisation/vpn-einrichtung.pdf]]),
- Webbrowser,
- Ihre Matrikelnummer,
- ein einfaches [[https://www.heise.de/newsticker/meldung/Gute-Passwoerter-erzeugen-und-sicher-verwenden-4295052.html][Passwort]], das Sie nirgendwo sonst verwenden.

** Vorbereitung
1. Wählen Sie sich ein einfaches Passwort und notieren Sie es an einem
   sicheren Ort.
2. Stellen sie die VPN-Verbindung zum Campusnetz der TH-Köln her.
3. Öffnen Sie die folgende Adresse in Ihren Webbrowser:
   #+begin_center
    [[http://f11-srv-phtheo.cit-f11.fh-koeln.de]]
   #+end_center
   Sie erhalten das folgende Anmeldefenster:
   #+ATTR_LATEX: :width 7cm
   [[./images/JupyterHub.png]]
4. Geben Sie Ihre Matrikelnummer in das Feld „Username“, das
   gewählte Passwort in das Feld „Password“ ein und klicken Sie auf
   „Sign In“. Sie sehen jetzt folgendes Bild:
   #+ATTR_LATEX: :width 7cm
   [[./images/JH_Files.png]]
5. Öffnen Sie das Verzeichnis „Pharmakokinetik“ durch einen Doppelklick:
   #+ATTR_LATEX: :width 7cm
   [[./images/JH_Pharmakokinetik.png]]
6. Öffnen Sie das Notebook =0_Einführung= mit einem Doppelklick und
   arbeiten es durch. (Dieses Notebook dient zu Ihrer Vorbereitung auf
   die weiteren Versuche. Wenn Sie sich bereits mit [[https://jupyter.org/][Jupyter]]-Notebooks
   und [[https://wwwpython.org][Python]] oder anderen imperativen Programmiersprachen auskennen,
   dürfen Sie diesen Schritt auslassen. Sie brauchen kein Protokoll zu
   diesem Notebook abzugeben.)

Weitere Informationen zum Arbeiten mit Jupyter finden Sie bei Bedarf
in dieser [[https://www.youtube.com/watch?v=tpLk-FC9kHI][Video-Einführung]] (4 min.) oder diesem [[https://docs.microsoft.com/de-de/azure/notebooks/tutorial-create-run-jupyter-notebook#tour-the-notebook-interface][Tutorial]].

Falls Sie bereits eine [[https://wiki.python.org/moin/IntegratedDevelopmentEnvironments][Python-Entwicklungsumgebung]] wie [[https://www.anaconda.com/products/individual][Anaconda]] auf
Ihrem Rechner installiert haben oder installieren möchten, können Sie
die Notebooks auch auf Ihren eigenen Rechner laden (Menu
/File/ \(\rightarrow\) /Download as/) und dort bearbeiten.

** Durchführung

Arbeiten Sie der Reihe nach die folgenden Jupyter-Notebooks
durch. 

- =1_Einkompartiment_IV= :: Einkompartimentmodell mit intravasaler
     Applikation\newline
     (/Injektion/, Kinetik 1. Ordnung und /Infusion/, 
     Kinetik 0. Ordnung),
- =2_Zweikompartiment_IV= :: Zweikompartimentmodell mit intravasaler
     Applikation,
- =3_Zweikompartiment_PO= :: Zweikompartimentmodell mit peroraler
     Einfach- und Mehrfachapplikation.

Lassen Sie die Befehle in den Code-Zellen durch den
Python-Interpreter ausführen, indem Sie Umschalt- und Enter-Taste
gleichzeitig drücken (Shift-Enter) oder /Run/ in der Werkzeugleiste
anklicken.
#+begin_center
[[./images/run.png]]
#+end_center

Am besten bearbeiten Sie die Aufgaben direkt in den Notebooks. Sie
können beliebig viele weitere Zellen mit Ihren Antworten oder
Diagrammen einfügen. Lassen Sie Python so viel wie möglich für Sie erledigen.

Achten Sie darauf, dass Sie das Notebook regelmäßig speichern (/Save
and Checkpoint/ im File-Menu oder Klick auf das
Diskettensymbol). Zusätzlich können Sie das bearbeitete Notebook auch
sichern, indem Sie eine Kopie auf Ihren eigenen Rechner laden
(/Download-as Notebook/ im File-Menu).

** Protokollabgabe

*** Python-Notebook

Wenn Sie fertig mit der Bearbeitung sind, laden Sie das Notebook
im /portable document format/ auf Ihren Rechner (/Download as PDF
via LaTeX/ im File-Menu) und reichen Sie es im Protokoll-Ordner auf
ILIAS unter der Übungseinheit „Versuch 09 – Pharmakokinetische
Simulationen“ ein.

*** Textverarbeitungsprogramm

Alternativ dürfen Sie das Protokoll zu den Aufgaben mit Ihrem
Lieblings-Textverarbeitungsprogramm schreiben.

Wenn Sie fertig mit der Bearbeitung sind, exportieren oder
konvertieren Sie Ihr Protokoll in das /portable document format/
und reichen Sie es im Protokoll-Ordner auf ILIAS unter der
Übungseinheit „Versuch 09 – Pharmakokinetische Simulationen“ ein.

* Weiterführende Literatur

[1] Bauer, Kurt, Frömming, Karl-Heinz und Führer, Claus, /Lehrbuch der
    Pharmazeutischen Technologie/, Hirzel-Verlag, 2016

[2] Voigt, Rudolf und Fahr, Alfred, /Pharmazeutische Technologie/,
    Hirzel-Verlag, 2015

[3] Schiffter, Heiko a., /Pharmakokinetik – Modelle und Berechnungen/,
    Wissenschaftliche Verlagsgesellschaft, 2015

[4] Urso, R., Blardi, P. und Giorgi, G., [[https://www.europeanreview.org/wp/wp-content/uploads/6.pdf][/A short introduction to pharmacokinetics/]],
    Eu Rev Med Pharmacol Sci, *6*, 2, 33-44, 2002
    
* Anlagen

- Notebook =1_Einkompartiment_IV= 
- Notebook =2_Zweikompartiment_IV= 
- Notebook =3_Zweikompartiment_PO= 

** Anpassung der Jupyter-TeX-Dateien                               :noexport:

1. =f11jupyter.sty= laden
2. Titelei

*** f11jupyter laden

- oberhalb von =\usepackage{iftex}

#+begin_src latex
    \usepackage{f11jupyter}
#+end_src

*** Titelei

**** Einkompartimentmodell – intravasale Applikation

#+begin_src latex
    \renewcommand{\maketitle}{\vspace*{0pt}}
    \fancyfoot[L]{\small Versuch 09\;–\;Anlage A\ (Einkompartimentmodell – intravasale Applikation)}
    \renewcommand\thesection{\Alph{section}}
    \setcounter{page}{10}
#+end_src

**** Zweikompartimentmodell – intravasale Applikation

#+begin_src latex
    \renewcommand{\maketitle}{\vspace*{0pt}}
    \fancyfoot[L]{\small Versuch 09\;–\;Anlage B\ (Zweikompartimentmodell – intravasale Applikation)}
    \renewcommand\thesection{\Alph{section}}
    \setcounter{section}{1}
    \setcounter{page}{15}
#+end_src

**** TODO Zweikompartimentmodell – perorale Applikation

#+begin_src latex
    \renewcommand{\maketitle}{\vspace*{0pt}}
    \fancyfoot[L]{\small Versuch 09\;–\;Anlage C\ (Zweikompartimentmodell – perorale Applikation)}
    \renewcommand\thesection{\Alph{section}}
    \setcounter{section}{2}
    \setcounter{page}{23}
#+end_src

